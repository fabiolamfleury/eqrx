# Drug Trials Application

This application implements management of Clinical Trials. During a clinical trial, it is important to ensure that sites administering the trial have the
appropriate amount of drug on site to give to the participants.

## How to use

### Requirements

In order to run the project, you need to have [docker](https://docs.docker.com/engine/install/) and [docker compose](https://docs.docker.com/compose/install/) installed.

### Commands

If this is your first time running you shoud use:

```
make build
```

Then, after build is completed, the application will be running already. If you want to return to it afterwards, you don't need to build again, simply run:

```
make run
```

Other useful commands:

```
make populate-db # populates database with indicated data from the exercise
make test # run implemented tests
```

## API DOCUMENTATION

The API can be accessed via (http://localhost:8000/)[http://localhost:8000/].

The routes available in this API and its methods are presented below:

- GET "/": API documentation, hyperlinking other routes
- GET "/drug-trials/": return a list of drug trials, including the sites that are participating, the drugs
involved, and the number of patients at each site
- POST "/drug-trials/": creates a new drug trial, needing to send information through json format. Example:
        ```{
                "name": "Drug Trial",
                "study_drug": "Drug Gama",
                "comparator_drug": "Drug Beta",
        }```
- GET "/trial-sites/": list all trial sites
- GET "/trial-sites/?frequency=daily": list drug sites that have daily shipments
- GET "/trial-sites/?frequency=weekly": list drug sites that have weekly shipments
- GET "/trial-sites/?frequency=monthly": list drug sites that have monthly shipments
- PATCH "/trial-sites/<int:pk>/": enroll a trial site of primary key pk in a drug trial. Example:
    "/trial-sites/1/", ```{"drug_trial": drug_trial_pk}```
