from django.urls import include, path
from rest_framework import routers
from trials import views

router = routers.DefaultRouter()

router = routers.DefaultRouter()
router.register(r"drug-trials", views.DrugTrialViewSet)
router.register(r"trial-sites", views.TrialSiteViewSet)

urlpatterns = router.urls
