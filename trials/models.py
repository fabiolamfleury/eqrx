from django.db import models
import math


class DrugTrial(models.Model):
    study_drug = models.CharField(max_length=240)
    comparator_drug = models.CharField(max_length=240)
    name = models.CharField(max_length=240)

    class Meta:
        ordering = ["-id"]

    def __str__(self) -> str:
        return self.name


class TrialSite(models.Model):
    DAILY = 1
    WEEKLY = 7
    MONTHLY = 30
    FREQUENCY_CHOICES = (
        (DAILY, "Daily"),
        (WEEKLY, "Weekly"),
        (MONTHLY, "Monthly"),
    )
    name = models.CharField(max_length=240)
    enrolled_patients = models.PositiveIntegerField()
    shipment_frequency = models.IntegerField(choices=FREQUENCY_CHOICES)
    drug_trial = models.ForeignKey(
        DrugTrial,
        related_name="trial_sites",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )

    class Meta:
        ordering = ["-id"]

    @property
    def each_drug_amount(self):
        return math.ceil(self.enrolled_patients / 2)

    def request_medicines(self):
        if self.drug_trial:
            study_drug_depot = self.get_medicine(self.drug_trial.study_drug)
            comparator_drug_depot = self.get_medicine(self.drug_trial.comparator_drug)
            return [study_drug_depot, comparator_drug_depot]
        return None

    def get_medicine(self, drug_name):
        medicine = Medicine.objects.filter(
            amount__gte=self.each_drug_amount, name=drug_name
        ).first()
        return medicine


class Depot(models.Model):
    name = models.CharField(max_length=240)


class Medicine(models.Model):
    name = models.CharField(max_length=240)
    depot = models.ForeignKey(Depot, on_delete=models.CASCADE)
    amount = models.PositiveIntegerField()

    class Meta:
        unique_together = ("name", "depot")
        ordering = ["-id"]
