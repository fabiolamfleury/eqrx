from django.test import TestCase
from rest_framework.test import APIClient
from trials.models import DrugTrial, TrialSite

from trials.serializers import DetailedTrialSiteSerializer, DrugTrialSerializer


class DrugTrialViewSetTestCase(TestCase):
    fixtures = [
        "trials/fixtures/initial_data.json",
    ]

    def setUp(self):
        self.client = APIClient()

    def test_create_drug_trial(self):
        drug_trial_name = "Study Trial"
        response = self.client.post(
            "/drug-trials/",
            {
                "name": drug_trial_name,
                "study_drug": "Drug Gama",
                "comparator_drug": "Drug Beta",
            },
            format="json",
        )
        assert response.status_code == 201
        assert DrugTrial.objects.filter(name=drug_trial_name).exists()

    def test_list_trials(self):
        response = self.client.get("/drug-trials/")

        serializer = DrugTrialSerializer(DrugTrial.objects.all(), many=True)
        assert response.status_code == 200
        assert response.data["results"] == serializer.data


class TrialSiteViewSetTestCase(TestCase):
    fixtures = [
        "trials/fixtures/initial_data.json",
    ]

    def setUp(self):
        self.client = APIClient()

    def test_list(self):
        response = self.client.get("/trial-sites/")

        serializer = DetailedTrialSiteSerializer(TrialSite.objects.all(), many=True)
        assert response.status_code == 200
        assert response.data == serializer.data

    def test_list_daily(self):
        response = self.client.get("/trial-sites/?frequency=daily")

        serializer = DetailedTrialSiteSerializer(
            TrialSite.objects.filter(shipment_frequency=TrialSite.DAILY), many=True
        )
        assert response.status_code == 200
        assert response.data == serializer.data

    def test_list_weekly(self):
        response = self.client.get("/trial-sites/?frequency=weekly")

        serializer = DetailedTrialSiteSerializer(
            TrialSite.objects.filter(shipment_frequency=TrialSite.WEEKLY), many=True
        )
        assert response.status_code == 200
        assert response.data == serializer.data

    def test_list_monthly(self):
        response = self.client.get("/trial-sites/?frequency=monthly")

        serializer = DetailedTrialSiteSerializer(
            TrialSite.objects.filter(shipment_frequency=TrialSite.MONTHLY), many=True
        )
        assert response.status_code == 200
        assert response.data == serializer.data

    def test_partial_update_enroll_trial(self):
        drug_trial = DrugTrial.objects.create(
            name="Trial B", study_drug="Drug Alpha", comparator_drug="Drug Beta"
        )
        response = self.client.patch(
            "/trial-sites/1/", {"drug_trial": drug_trial.pk}, format="json"
        )
        updated_trial_site = TrialSite.objects.get(pk=1)
        serializer = DetailedTrialSiteSerializer(updated_trial_site)
        assert response.status_code == 200
        assert response.data == serializer.data
        assert updated_trial_site.drug_trial == drug_trial

    def test_invalid_partial_update(self):
        response = self.client.patch(
            "/trial-sites/1/", {"something_invalid": "text"}, format="json"
        )
        assert response.status_code == 400
