# Generated by Django 4.1 on 2022-08-05 16:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Depot",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=240)),
            ],
        ),
        migrations.CreateModel(
            name="DrugTrial",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("study_drug", models.CharField(max_length=240)),
                ("comparator_drug", models.CharField(max_length=240)),
                ("name", models.CharField(max_length=240)),
            ],
            options={
                "ordering": ["-id"],
            },
        ),
        migrations.CreateModel(
            name="TrialSite",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=240)),
                ("enrolled_patients", models.PositiveIntegerField()),
                (
                    "shipment_frequency",
                    models.IntegerField(
                        choices=[(1, "Daily"), (7, "Weekly"), (30, "Monthly")]
                    ),
                ),
                (
                    "drug_trial",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.PROTECT,
                        related_name="trial_sites",
                        to="trials.drugtrial",
                    ),
                ),
            ],
            options={
                "ordering": ["-id"],
            },
        ),
        migrations.CreateModel(
            name="Medicine",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=240)),
                ("amount", models.PositiveIntegerField()),
                (
                    "depot",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="trials.depot"
                    ),
                ),
            ],
            options={
                "ordering": ["-id"],
                "unique_together": {("name", "depot")},
            },
        ),
    ]
