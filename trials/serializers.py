from rest_framework import serializers
from trials.models import DrugTrial, Medicine, TrialSite


class TrialSiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = TrialSite
        fields = ["name", "enrolled_patients", "shipment_frequency"]


class DetailedTrialSiteSerializer(serializers.ModelSerializer):
    medicines = serializers.SerializerMethodField()
    each_drug_amount = serializers.SerializerMethodField()
    drug_trial = serializers.StringRelatedField()

    class Meta:
        model = TrialSite
        fields = ["name", "drug_trial", "each_drug_amount", "medicines"]

    def get_medicines(self, obj):
        medicines = obj.request_medicines()
        serializer = MedicineSerializer(medicines, many=True)
        return serializer.data

    def get_each_drug_amount(self, obj):
        return obj.each_drug_amount


class DrugTrialSerializer(serializers.ModelSerializer):
    trial_sites = TrialSiteSerializer(many=True, read_only=True)

    class Meta:
        model = DrugTrial
        fields = ["name", "study_drug", "comparator_drug", "trial_sites"]


class MedicineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medicine
        fields = ["name", "depot"]
