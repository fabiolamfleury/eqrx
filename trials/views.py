from rest_framework import mixins, viewsets, status
from rest_framework.response import Response

from trials.models import DrugTrial, TrialSite
from trials.serializers import DrugTrialSerializer, DetailedTrialSiteSerializer


class DrugTrialViewSet(
    mixins.ListModelMixin, mixins.CreateModelMixin, viewsets.GenericViewSet
):
    queryset = DrugTrial.objects.all()
    serializer_class = DrugTrialSerializer


class TrialSiteViewSet(
    mixins.ListModelMixin, mixins.UpdateModelMixin, viewsets.GenericViewSet
):
    queryset = TrialSite.objects.all()
    serializer_class = DetailedTrialSiteSerializer

    def list(self, request):
        params = request.query_params
        if "frequency" in params:
            frequency = getattr(TrialSite, params["frequency"].upper())
            trial_sites = TrialSite.objects.filter(shipment_frequency=frequency)
            serializer = self.serializer_class(trial_sites, many=True)
            return Response(serializer.data)

        serializer = self.serializer_class(self.queryset, many=True)
        return Response(serializer.data)

    def partial_update(self, request, *args, **kwargs):
        if "drug_trial" in request.data and request.data["drug_trial"]:
            drug_trial = DrugTrial.objects.get(pk=request.data["drug_trial"])
            trial_site = TrialSite.objects.get(pk=kwargs["pk"])
            trial_site.drug_trial = drug_trial
            trial_site.save()
            return Response(self.serializer_class(trial_site).data)
        content = "Partial update can only be for drug trial enrollment."
        return Response(content, status=status.HTTP_400_BAD_REQUEST)
