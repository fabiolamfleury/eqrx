populate-db:
	docker-compose exec server python manage.py loaddata initial_data.json

test:
	docker-compose exec server python manage.py test

run:
	docker-compose exec server python manage.py migrate
	docker-compose up -d

build:
	docker-compose up -d --build
	docker-compose exec server python manage.py migrate